#include<stdio.h>
#include<stdlib.h>  //malloc() and free() need this header file!
#include<string.h>

#include"../include/housie.h"

int main()
{
	bts_status_t _status = BTS_STATUS_OK;
	bts_ticket_t* tix  = bts_readFromFile("../resources/ticket.txt",&_status);
	bts_bool_t isValid = bts_isMyTicketValid(tix,NULL);

	bts_rulebook_t rb = {0};
	rb.numberOfRules = 8;
	/*
	4 corners
	1st line
	2n line 
	3rd line 
	full Housie!
	*/
	
	//for 4 corners => 0,4,10,14
	
	strcpy(&rb.ruleName[0][0],"four corners");
	strcpy(&rb.ruleName[1][0],"first line");
	strcpy(&rb.ruleName[2][0],"second line");
	strcpy(&rb.ruleName[3][0],"third line");
	strcpy(&rb.ruleName[4][0],"Full Housie!");
	strcpy(&rb.ruleName[5][0],"King's corner");
	strcpy(&rb.ruleName[6][0],"Queen's corner");
	strcpy(&rb.ruleName[7][0],"Bade miya Chhote miya");
	rb.isRuleActive[0] = enBoolTrue;
	rb.isRuleActive[1] = enBoolTrue;
	rb.isRuleActive[2] = enBoolTrue;
	rb.isRuleActive[3] = enBoolTrue;
	rb.isRuleActive[4] = enBoolTrue;
	rb.isRuleActive[5] = enBoolTrue;
	rb.isRuleActive[6] = enBoolTrue;
	rb.isRuleActive[7] = enBoolFalse;
	
	
	//4 corners rule
	rb.rule[0][0] = enBoolTrue;
	rb.rule[0][4] = enBoolTrue;
	rb.rule[0][10] = enBoolTrue;
	rb.rule[0][14] = enBoolTrue;
	rb.rule[5][0] = enBoolTrue;
	rb.rule[5][5] = enBoolTrue;
	rb.rule[5][10] = enBoolTrue;
	rb.rule[6][4] = enBoolTrue;
	rb.rule[6][9] = enBoolTrue;
	rb.rule[6][14] = enBoolTrue;
	rb.rule[7][0] = enBoolTrue;
	rb.rule[7][9] = enBoolTrue;
	
	int i;
	for(i=0;i<15;i++)
	{
		
		if(i<5) //first line
		{
			rb.rule[1][i] = enBoolTrue;
		}
		
		if((i>=5)&&(i<10)) //2nd line
		{
			rb.rule[2][i] = enBoolTrue;
		}
		
		if((i>=10)&&(i<15)) //2nd line
		{
			rb.rule[3][i] = enBoolTrue;
		}
		rb.rule[4][i] = enBoolTrue;
	}
	
	int userInput = 0;
	bts_bool_t tally;
	int ruleNumber = -1;
	char anyKey =0;
	
	system("cls");

	if(isValid==enBoolFalse)
	{
		printf("\n[error] : ticket seems to be invalid! please try again!\n");
	}else
	{
		printf("\n[info] : ticket loaded successfully");
		do{
			printf("\n\n\n---HOUSIE-PROGRAM---\n");
			if(tix==NULL)
			{
				printf("[error] : ticket not found!\n");
				break;
			}else
			{
				bts_printMyTicket(tix);
				printf("\nEnter the number between (0-99) : ");
				scanf("%d",&userInput);
				
				if((userInput>=0)&&(userInput<100))
				{
					tally = bts_addTally(tix,userInput,NULL);
					if(tally==enBoolTrue)
					{
						printf("\ntallied : %d",userInput);
						ruleNumber = bts_didIWin(&rb,tix,NULL);
						if(ruleNumber>-1)
						{
							rb.isRuleActive[ruleNumber] = enBoolFalse;
							printf("\n\nYou Won the %s!!!\n\n",rb.ruleName[ruleNumber]);
						}
					}
				}			
			}
			while((anyKey = getchar()) != '\n' && anyKey != EOF); //flush the input buffer;
			anyKey = getchar();
			system("cls");
		}while((userInput>=0)&&(userInput<100));
	}
	printf("\nThank you for using the Housie Program!");
	getchar();
	bts_ticket_delete(tix); //cleaning after use
	return 0;
}
