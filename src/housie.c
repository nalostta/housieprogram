#include"..\include\housie.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

bts_ticket_t*    bts_ticket_new()
{
	bts_ticket_t* tix = malloc(sizeof*tix); //calloc
	
	int i;
	for(i=0;i<27;i++) //0->26 = 27 times
	{
		if(i<15)
		{
			tix->checked[i]=enBoolFalse;
		}
		tix->lines[i]=0;
	}
	return tix;
}

void bts_ticket_delete(bts_ticket_t* tix)
{
	free(tix);
}

bts_ticket_t* bts_readFromFile(const char* fileName,bts_status_t* status)
{
	bts_status_t _status = BTS_STATUS_OK;
	bts_ticket_t* newTicket = bts_ticket_new();
	
	if(fileName==NULL)
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		FILE* tixFile = fopen(fileName,"r"); //to read, you use "r", for writing use "w"
		if(tixFile==NULL)	
		{
			_status = BTS_STATUS_MEMORY_READ_FAILED; //fopen returns null when it is not able to open the textfile!
		}else
		{
			//so if the control reaches till here,then
			//the inputs are proper and the text file has been opened successfully!
			//now we will require buffer 
			char buffer[50]; 
			if(fgets(buffer,50,tixFile)==NULL)
			{
				//this means fgets could not read the line properly
				//this is an error condition 
				//so we need to clean up and return error to whoever called this fnmerge
				_status = BTS_STATUS_MEMORY_READ_FAILED;
				bts_ticket_delete(newTicket);
				newTicket=NULL;
			}else
			{
				bts_getSequence(&newTicket->lines[0],buffer,50); //this shall give the first row
				
				
				if(fgets(buffer,50,tixFile)==NULL)  //checks whether the fgets fn has read successfully
				{
					_status = BTS_STATUS_MEMORY_READ_FAILED;
					bts_ticket_delete(newTicket);
					newTicket=NULL;
				}else
				{
					bts_getSequence(&newTicket->lines[9],buffer,50); //second row
					
					
					if(fgets(buffer,50,tixFile)==NULL)  //checks whether the fgets fn has read successfully
					{
						_status = BTS_STATUS_MEMORY_READ_FAILED;
						bts_ticket_delete(newTicket);
						newTicket=NULL;
					}else
					{
						bts_getSequence(&newTicket->lines[18],buffer,50); //third row
						//so all the 3 lines have been read successfully into the ticket.
					}
				}
			}
		}
	}
	if(status!=NULL)
	{
		(*status) = _status;
	}
	
	return newTicket;
	
}

bts_status_t bts_getSequence(int* output,const char* line,unsigned int maxBufferSize)
{
	bts_status_t _status = BTS_STATUS_OK;
	
	//This function will take the character array from the buffer as it's input and read all the numbers from it.
	if((line==NULL)||(output==NULL))
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		int num_i,char_i=0; //remember 'i' that we use in for loops? this is similar but with more context.
		char temp[2]; 		//we read 2 digit numbers from our ticket,this will store the 2 characters
		//  
		for(num_i=0;num_i<9;num_i++) //9 times for 9 numbers in a row
		{
			while(line[char_i]!='[')
			{
				char_i++;
				if(char_i>=maxBufferSize)
				{
					_status = BTS_STATUS_ERROR;
					break;
				}
			} //gets the iterator till the character '['
			
			temp[0] = line[char_i+1]; //the next two characters are supposed to numbers
			temp[1] = line[char_i+2];
			
			output[num_i] = bts_char2Int(temp,NULL); //convert the 2 ASCII characters into numbers and store it in sequence
			char_i++;
		}
	}
	return _status;
}

int bts_char2Int(const char* string,bts_status_t* status)
{
	bts_status_t _status = BTS_STATUS_OK; //
	int output = 0;
	if(string==NULL)
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		if(((string[0]>='0')&&(string[0]<='9'))&&((string[1]>='0')&&(string[1]<='9')))
		{
			output = ((string[0]-ASCII_NUMBER_START_OFFSET)*10)+(string[1]-ASCII_NUMBER_START_OFFSET);
		}else
		{
			output = -1;
		}
	}
	if(status!=NULL)
	{
		(*status) = _status;
	}
	
	return output;
}

bts_bool_t bts_addTally(bts_ticket_t* tix,int num,bts_status_t* status)
{
	bts_status_t _status = BTS_STATUS_OK;
	bts_bool_t numExists = enBoolFalse; 		//we will initially assume that the input number doesn't exist,if it does then we will change this value and return it's index
	
	if(tix==NULL)
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		if((num<0)||(num>100))	 				//we know that housie numbers are between 0 & 100 thus for an input outside that range and the function will return the status => invalid_argument!
		{
			_status = BTS_STATUS_INVALID_ARG;
		}else
		{
			int cell_i = 0;						//there are 27 cells in a housie ticket (numbered and blanks)
			int num_i  = 0; 					//there are 15 numbers in a housie ticket			
			
			for(cell_i=0;cell_i<27;cell_i++)	//this will loop through the 27 cells
			{
				if(tix->lines[cell_i]==num)
				{
					numExists = enBoolTrue;
					tix->checked[num_i] = enBoolTrue;
					break;
				}else
				if(tix->lines[cell_i]!=-1)
				{
					num_i++;
				}
			}
			
			if(numExists!=enBoolTrue)
			{
				tix->checked[num_i] = enBoolTrue;
			}
		}
	}
	
	if(status!=NULL)
	{
		(*status) = _status;
	}
	
	return numExists;
}

int bts_didIWin(bts_rulebook_t* ruleBook,bts_ticket_t* myTicket,bts_status_t* status) 	//returns the index to the 'First' rule which matched the housie,but if I didn't win any then returns -1
{
	bts_status_t _status = BTS_STATUS_OK;
	int ruleNum = -1; 	//-1 means no housie won yet
	
	if((ruleBook==NULL)||(myTicket==NULL))
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		int rule_i;
		int cell_i;
		int num_i;
		bts_bool_t ruleMatch; 
		
		for(rule_i=0;rule_i<ruleBook->numberOfRules;rule_i++)
		{
			if(ruleBook->isRuleActive[rule_i]==enBoolTrue) 	//if the particular rule at the index 'rule_i' is active then only check it with the ticket
			{
				ruleMatch = enBoolTrue; 					//false till found true
				
				for(num_i=0;num_i<15;num_i++)
				{
					if((ruleBook->rule[rule_i][num_i]==enBoolTrue)&&(myTicket->checked[num_i]!=enBoolTrue))
					{
						ruleMatch = enBoolFalse;
					}
				}
			}else
			{
				continue;   				//this 'else' part is just a formality...
			}
			
			if(ruleMatch==enBoolTrue)
			{
				//printf("\nyou won! rule number : %d\n",rule_i);
				ruleNum = rule_i;
				break; 						//if a rule matches,break out of this loop, no need to check for other rules
			}
		}
	}
	if(status!=NULL)
	{
		(*status) = _status;
	}
	
	return ruleNum;
}

void bts_printMyTicket(const bts_ticket_t* myTicket)
{
	printf("\nTicket :");
	if(myTicket==NULL)
	{
		printf(" not Found!");
	}else
	{
		printf("\n\n|-----------------------------------------------------------------------");
		int i;
		int num=0;
		for(i=0;i<27;i++)
		{
			if(i%9==0)
			{
				printf("|\n| ");
			}
			if(myTicket->lines[i]==-1)
			{
				printf("X\t");
			}else
			{
				printf("%d",myTicket->lines[i]);
				if(myTicket->checked[num]==enBoolTrue)
				{
					printf("$");
				}
				printf("\t");
				num++;
			}
		}
		printf("|\n|-----------------------------------------------------------------------|\n\n");
	}
}

bts_bool_t bts_isMyTicketValid(bts_ticket_t* tix,bts_status_t* status)
{
	bts_status_t _status = BTS_STATUS_OK;
	bts_bool_t   isValid  = enBoolTrue;
	
	
	if(tix==NULL)
	{
		_status = BTS_STATUS_NULL_ARG;
	}else
	{
		//execute all the ticket-validity fns here...
		int numSeq[15]; //
		int col_i;
		int num_i=0;
		
		for(col_i=0;col_i<9;col_i++)  									//traverses column wise
		{	
			if((tix->lines[col_i]>=0)&&(tix->lines[col_i]<=100)) 		//row 1
			{
				numSeq[num_i] = tix->lines[col_i];
				num_i++;
			}
			
			if((tix->lines[col_i+9]>=0)&&(tix->lines[col_i+9]<=100)) 	//row 2
			{
				numSeq[num_i] = tix->lines[col_i+9];
				num_i++;
			}
			
			if((tix->lines[col_i+18]>=0)&&(tix->lines[col_i+18]<=100)) 	//row 3
			{
				numSeq[num_i] = tix->lines[col_i+18];
				num_i++;
			}
		}
		
		if(num_i!=15) //check whether 15 valid numbers have been read!
		{
			isValid = enBoolFalse;
		}
		for(num_i=0;num_i<14;num_i++)
		{
			
			if(numSeq[num_i]>=numSeq[num_i+1])
			{
				isValid = enBoolFalse;
				break;
			}
		}
		
	}
	return isValid;
}
