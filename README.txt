******************************************************
*												   	 *
*					Housie Program				     *
*													 *	
*													 *
******************************************************
author : nalostta

This project was developed as an example program for a
webinar conducted under team DJS Robocon, D.J.Sanghvi 
college of engineering, held on 9,10 & 11th of May,2020.
The Main executable resides in the bin directory.While 
the repository contains an already built executable,if 
it doesn't work, then you can build the executable from 
the source code.You will need GNU-gcc and make to build
from scratch.
	The ticket can be edited by changing the contents
of 'ticket.txt' in the resources folder.
	You are free to use/modify the source code as per 
your needs.
for any queries,
contact : shah.manthan38@gmail.com