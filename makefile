CC = gcc
FLAGS = -g -o

all : bin/housieProgram.exe

bin/housieProgram.exe : objects/main.o libraries/libHousie.a
	gcc $(FLAGS) $@ $^

objects/%.o : src/%.c 
	$(CC) -c $(FLAGS) $@ $<
	
libraries/libHousie.a : objects/housie.o include/housie.h include/types.h include/defs.h
	ar rcs -o libraries/libHousie.a objects/housie.o

clean :
	rm -f bin/housieProgram.exe
	rm -f objects/main.o
	rm -f objects/housie.o
	rm -f libraries/libHousie.a
	
.phony : all
.phony : clean