#ifndef __BTS_INCLUDED_HOUSIE_H__
#define __BTS_INCLUDED_HOUSIE_H__

#include"types.h"

//module functions
bts_ticket_t*	bts_ticket_new();  		//creates a new ticket
void 		    bts_ticket_delete(bts_ticket_t* tix); 	//deletes the ticket

//module attributes
bts_ticket_t*	bts_readFromFile(const char* fileName,bts_status_t* status);
bts_status_t	bts_getSequence(int* output,const char* line,unsigned int maxBufferSize);	//using unsigned int for buffer size actually saves me the trouble of checking whether it is negative coz u know the datatype itself doesn't permit so.
bts_bool_t    	bts_addTally(bts_ticket_t* tix,int num,bts_status_t* status); 				//will check and add a tally to the num and return a boolean value if true, 'bts_bool_t' is just the name of the boolean type i have chosen,it can be different but at the core it is an enum only
bts_bool_t		bts_isMyTicketValid(bts_ticket_t* myTicket,bts_status_t* status);
int 			bts_didIWin(bts_rulebook_t* ruleBook,bts_ticket_t* myTicket,bts_status_t* status);
int 		    bts_char2Int(const char* string,bts_status_t* status);
void       		bts_printMyTicket(const bts_ticket_t* myTicket);

#endif