#ifndef __BTS_INCLUDED_TYPES_H__
#define __BTS_INCLUDED_TYPES_H__

#include"defs.h"

typedef unsigned int bts_status_t;  //typedef keyword 'aliases' a datatype to another name example : typedef int carrot; -> now you can treat 'carrot' as a new name for int...you can still int for your work but doing this introduces context.
 
typedef enum
{
	enBoolFalse,
	enBoolTrue
}bts_bool_t; 

typedef struct        
{
	int numberOfRules;    				
	bts_bool_t rule[10][15]; 
	bts_bool_t isRuleActive[10];	        //once a ticket wins a rule, that rule must be de-activated (meaning the didIwin fn will ignore this rule)...otherwise the program will display the win message after every number entered.
	char ruleName[10][ruleNameMaxSize];    //there are no string types in C, string is a character array & this is an 'array of strings' .........also multi-dimensional pointers.
}bts_rulebook_t;

typedef struct
{
	int lines[27];				   		//stores the main housie ticket
	bts_bool_t checked[15]; 			//stores the tallies of the numbers,it is of boolean type meaning it can only store 2 values as we defined in the enum;
}bts_ticket_t; 

#endif