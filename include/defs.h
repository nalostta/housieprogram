#ifndef __BTS_INCLUDED_DEFS_H__
#define __BTS_INCLUDED_DEFS_H__

#define BTS_STATUS_OK 						  0
#define BTS_STATUS_ERROR 					 -1
#define BTS_STATUS_NULL_ARG					 -2
#define BTS_STATUS_INVALID_ARG       		 -3
#define BTS_STATUS_MEMORY_ERROR 			 -4
#define BTS_STATUS_MEMORY_ALLOCATION_FAILED  -5
#define BTS_STATUS_MEMORY_READ_FAILED 		 -6

#define BTS_FIRST_ROW_START		0
#define BTS_SECOND_ROW_START	9
#define BTS_THIRD_ROW 			18

#define ASCII_NUMBER_START_OFFSET 48
#define ruleNameMaxSize 20

#endif